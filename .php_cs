<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude('vendor')
    ->exclude('Tests/resources')
    ->in(__DIR__);

return PhpCsFixer\Config::create()->setFinder($finder);
