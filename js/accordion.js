/**
 * Created by turnerc on 6/10/2014.
 */
// remap jQuery to $
(function ($) {
})(window.jQuery);

jQuery(function ($) {
        "use strict";

        function accordionToggle() {
            $("#collapsible-container").accordion({
                header: ".collapsible-item-header",
                collapsible: true,
                heightStyle: "content",
                active: false,
                animate: {
                    duration: 500
                },
                icons: {
                    header: "fa fa-plus",
                    activeHeader: "fa fa-minus"
                }
            });
        }

        /* trigger when page is ready */
        $(document).ready(function () {
            accordionToggle();
        });
    }
);
