/**
 * Created by turnerc on 6/10/2014.
 */
// remap jQuery to $
(function ($) {
})(window.jQuery);

jQuery(function ($) {
        "use strict";

        function panelToggle() {
            $('.collapsible-item-header').click(function () {
                $(this).next().slideToggle(500);
                $(this).closest('.collapsible-item-header').toggleClass('panel-expand');
                return false;
            }).next().hide();
        }

        function keyboardToggle() {
            $(".collapsible-item-header").keydown(function (event) {
                if (event.keyCode == 13 || event.keyCode == 32) {
                    $(this).next().slideToggle(500);
                    $(this).closest('.collapsible-item-header').toggleClass('panel-expand');
                    return false;
                }
            });
        }

        /* trigger when page is ready */
        $(document).ready(function () {
            panelToggle();
            keyboardToggle();
        });
    }
);
