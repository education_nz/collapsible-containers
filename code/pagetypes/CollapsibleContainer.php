<?php

/**
 * Created by PhpStorm.
 * User: turnerc
 * Date: 6/10/2014
 * Time: 1:30 PM
 */
class CollapsibleContainer extends Page
{
    private static $singular_name = "Collapsible container";

    private static $plural_name = "Collapsible containers";

    private static $description = "Collapsible container for collapsible items";

    private static $can_be_root = true;

    private static $allowed_children = array('CollapsibleItem');

    private static $icon = 'collapsible-containers/img/icons/sitetree_images/accordion-container-icon.png';

    private static $default_child = "CollapsibleItem";

    private static $db = array(
        'TypeOfContainer' => "Enum(array('accordion', 'panel'))",
        'BelowContent'    => 'HTMLText',
    );

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->addFieldsToTab(
            "Root.Main",
            array(
                OptionsetField::create("TypeOfContainer", "Type of container", array("accordion" => "Automatically collapse", "panel" => "Click to collapse"))
            ),
            'Content'
        );

        $fields->addFieldsToTab(
            "Root.Main",
            array(
                HtmlEditorField::create('Content', 'Content above accordion'),
                HtmlEditorField::create('BelowContent', 'Content below accordion')
            ),
            "Metadata"
        );

        return $fields;
    }

    public function getCMSValidator()
    {
        return new RequiredFields('TypeOfContainer');
    }
}

class CollapsibleContainer_Controller extends Page_Controller
{
    public function init()
    {
        parent::init();

        if ($this->TypeOfContainer == "accordion") {
            Requirements::css(MODULE_DIR . "/css/accordion.css");
            Requirements::javascript(MODULE_DIR . "/js/lib/jquery-ui-accordion.js");
            Requirements::javascript(MODULE_DIR . "/js/accordion.js");
        } else {
            Requirements::css(MODULE_DIR . "/css/panel.css");
            Requirements::javascript(MODULE_DIR . "/js/panel.js");
        }
    }

    public function getCollapsibleItem()
    {
        return SiteTree::get()->filter(array('ClassName' => 'CollapsibleItem', 'ParentID' => $this->ID));
    }
}
