<?php

/**
 * Created by PhpStorm.
 * User: turnerc
 * Date: 7/10/2014
 * Time: 9:10 AM
 */
class CollapsibleItem extends Page
{
    private static $singular_name = "Collapsible item";

    private static $plural_name = "Collapsible items";

    private static $description = "Each item will collapse and expand";

    private static $allowed_children = "none";

    private static $can_be_root = false;

    private static $icon = 'collapsible-containers/img/icons/sitetree_images/accordion-item-icon.png';

    private static $db = array(
        'Title'   => 'Varchar(255)',
        'Content' => 'HTMLText',
    );

    private static $defaults = array(
        'ShowInMenus'       => false,
        'DisplayInMegaMenu' => false,
    );

    public function getBaseUrl()
    {
        return Director::BaseURL();
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->addFieldsToTab("Root.Main", array(
            TextField::create('Title', 'Title'),
            HtmlEditorField::create('Content', 'Content'),
        ));

        $fields->removeByName('Translations');
        $fields->removeByName('PublishingSchedule');
        $fields->removeByName('RelatedPages');
        $fields->removeByName('URLSegment');
        $fields->removeByName('MenuTitle');

        return $fields;
    }
}

class CollapsibleItem_Controller extends CollapsibleContainer_Controller
{
}
