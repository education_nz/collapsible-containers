module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            sass: {
                files: ['sass/**/*.{scss,sass}', 'sass/_partials/**/*.{scss,sass}'],
                tasks: ['sass:dist']
            }
        },
        sass: {
            options: {
//                sourceComments: 'map'
                outputStyle: 'compressed'
            },
            dist: {
                files: {
                    'css/accordion.css': 'sass/accordion.scss',
                    'css/panel.css': 'sass/panel.scss'
                }
            }
        }
    });
    grunt.registerTask('default', ['sass:dist', 'watch']);
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
};