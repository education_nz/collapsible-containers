<?php
/**
 * Created by PhpStorm.
 * User: turnerc
 * Date: 6/10/2014
 * Time: 4:41 PM
 */

// absolute module directory path
if (!defined('MODULE_PATH')) {
    define('MODULE_PATH', dirname(__FILE__));
}

// module directory name used for Requirements
if (!defined('MODULE_DIR')) {
    define('MODULE_DIR', basename(MODULE_PATH));
}
